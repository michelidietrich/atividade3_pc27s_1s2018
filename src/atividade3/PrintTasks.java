/**
 * Exemplo1: Programacao com threads
 * Autor: Micheli Leticia Dietrich
 * Ultima modificacao: 07/08/2017
 */
package atividade3;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 *
 * @author Micheli
 */
public class PrintTasks implements Runnable {

   private String nome;
   private String cor;
   private String hora;
    
    public PrintTasks (String nome, String cor){
        this.nome = nome;
        this.cor = cor;
        
        
    }
    public void run(){
        
   
        while (true){
    
              switch (cor){
            case "verde":
                this.hora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                this.cor = "amarelo";
                System.out.printf("Nome %s cor % Hora %s" + this.nome, this.cor, this.hora);
                break;
             
            case "amarelo":
                System.out.printf("Nome %s cor % Hora %s" + this.nome, this.cor, this.hora);
                this.cor = "vermelho";
                break;
             
            case "vermelho":
                System.out.printf("Nome %s cor % Hora %s" + this.nome, this.cor, this.hora);
                this.cor = "verde";
                break;
            
            default:
                break;
              }//fim switch
        }// fim while
        
}   
}
